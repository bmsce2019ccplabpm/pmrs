#include <stdio.h>
int input()
{
    int a;
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}

void input1(int n, int a[10])
{
    int i;
    printf("Enter the elements of the array\n");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
}

int sumd(int n, int a[10])
{
    int sum=0;
    for(int i=0;i<n;i++) 
    {
        sum=sum+a[i];
    }
    return sum;
}

void output(int n, int a[10], int sum)
{
    int i;
    printf("The sum of "); 
    for(i=0;i<n-1;i++) 
    {
        printf("%d+",a[i]);
    }
    printf("%d=%d",a[i],sum);
}

int main()
{
    int n,sum,a[10];
    n=input();
    input1(n,a);
    sum=sumd(n,a);
    output(n,a,sum);
    return 0;
}
