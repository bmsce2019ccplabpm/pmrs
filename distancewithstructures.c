#include<stdio.h>
#include<math.h>
struct point
{
 float x;
 float y;
};
typedef struct point Point;
Point point_input()
{
 Point p;
 printf("enter x,y coordinates of point \n");
 scanf("%f %f",&p.x,&p.y);
 return p;
}
float find_distance(Point p1,Point p2)
{
 float d;
 d=sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p2.y-p1.y)*(p2.y-p1.y));
 return d;
}
void output(Point p1,Point p2,float d)
{
 printf("distance between %f,%f and %f%f is %f",p1.x,p1.y,p2.x,p2.y,d);
}
int main()
{
 Point p1,p2;
 float d;
 p1=point_input();
 p2=point_input();
 d=find_distance(p1,p2);
 output(p1,p2,d);
 return 0;
} 