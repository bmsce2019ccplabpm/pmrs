#include <stdio.h>
int hc(int a,int b)
{
    if(a==0)
        return b;
    return hc(b%a,a);
}
typedef struct
{
    int  p;
    int q;
} fraction;
void output(fraction a,int n,fraction b[n],fraction c)
{
    int i;
    for(i=0;i<n-1;i++)
    {
     printf("%d/%d+",b[i].p,b[i].q);
    }
    printf("%d/%d =%d/%d\n",b[i].p,b[i].q,c.p,c.q);
}
int main()
{
    int n;
    fraction a,c;
    printf("Enter number of fraction you want to add\n");
    scanf("%d",&n);
    fraction b[n];
    for(int i=0;i<n;i++)
    {
     printf("Enter numerator & denominator of the fraction\n");
     scanf("%d%d",&b[i].p,&b[i].q);
    }
    a.p=0;
    a.q=1;
    for(int i=0;i<n;i++)
    {
     c.q=hc(a.q,b[i].q);
     c.q=a.q*b[i].q/c.q;
     c.p=a.p*(c.q/a.q)+b[i].p*(c.q/b[i].q);
     a.p=c.p;
     a.q=c.q;
    }
    output(a,n,b,c);
    return 0;
}
