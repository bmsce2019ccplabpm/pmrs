#include<stdio.h>
#include<math.h>
float input()
{
 float a;
 printf("Enter a number\n");
 scanf("%f",&a);
 return a;
}
float distance(float x1,float y1,float x2,float y2)
{
 float d=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
 return d;
}
void output(float x1,float y1,float x2,float y2,float d)
{
printf("Distance between %f,%f and %f,%f is %f\n",x1,y1,x2,y2,d);
}
int main()
{
float x1=input();
float x2=input();
float y1=input();
float y2=input();
float d;
d=distance(x1,x2,y1,y2);
output(x1,x2,y1,y2,d);
return 0;
}