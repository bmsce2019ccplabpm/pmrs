#include <stdio.h>
int input()
{
 int n;
 printf("Enter the number:\n");
 scanf("%d",&n);
 return n;
}    
int sumd(int n)
{
 int i,sum=0,r;
 for(i=n;i!=0;i=i/10)
 {
  r=i%10;
  sum=sum+r;
 }
 return sum;
}
void output(int sum)
{
 printf("The sum of digits=%d\n",sum);
}
int main()
{
 int n,sum;
 n=input();
 sum=sumd(n);
 output(sum); 
 return 0;
}    
